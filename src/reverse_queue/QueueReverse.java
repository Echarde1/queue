package reverse_queue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class QueueReverse {

    private static Queue<Integer> queue;

    private static Queue<Integer> reverseQueue(Queue<Integer> q) {
        if (q.isEmpty())
            return q;

        int element = q.remove();

        q = reverseQueue(q);
        q.add(element);

        return q;
    }

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        queue = new LinkedList<>();

        while (scn.hasNextInt()) {
            queue.add(scn.nextInt());
        }

        queue = reverseQueue(queue);

        while (!queue.isEmpty()) {
            System.out.print(queue.remove() + " ");
        }
    }
}
