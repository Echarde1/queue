package reverse_queu_part;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class QueueReversePartly {
    private static Stack<Integer> stack;
    private static Queue<Integer> queue;

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int k = scn.hasNextInt() ? scn.nextInt() : 0;

        initQueueAndStack(scn);

        reverseQueuePartly(k);
    }

    private static void initQueueAndStack(Scanner scn) {
        queue = new LinkedList<>();
        stack = new Stack<>();

        while (scn.hasNextInt()) {
            queue.add(scn.nextInt());
        }
    }

    private static void reverseQueuePartly(int k) {
        for (int i = 0; i < k; i++) {
            int element = queue.remove();
            stack.push(element);
        }

        while (!stack.isEmpty()) {
            int element = stack.pop();
            queue.add(element);
        }

        for (int i = 0; i < queue.size() - k; i++) {
            int element = queue.remove();
            queue.add(element);
        }

        System.out.println(queue.toString());
    }
}
